# Introduction

These are a set of scripts to download files to create an environment
where you can build boot images for the Radxa Rock Pro board.

Note that it will download gigabytes of files and hence make sure you
have the internet traffic quota and diskspace to support the scripts.

# Getting Started

If you just want to download the files, just run
```
./download_sources.sh
```
However, if you want it to download and build the kernels you can run
```
./linux-stable.sh
```

# Directory structure

The scripts will create a number of directories in the working
directories. Below is the tables of some of the directories and what
they are used for.

Directory | Description
----------|-------------
downloads | where all the downloads are stored
parameter | NAND partition configuration files for various setup
build     | where the images for flash will be placed when built
scripts   | directory containing bash functions for the scripts

# Details

## Building the Linux kernel

This repository will download and create at least three Linux kernel
source directory from offical repos or zip file:

  1. linux-rockchip
  2. linux-stable
  3. linux-next

The following bash commands are to build the kernel manually. We
assume you are executing them from with the any of the above Linux
kernel directories. To automatically build the kernels, use the
```sh``` scripts in the base directory.

To a complete clean of the kernel sources

```
[-d modules ] && rm -rf modules
make clean
make distclean
cp ../$(basename $(pwd)).defconfig arch/arm/configs/radxarock_custom_defconfig
. ../scripts/toolchain.sh
make radxarock_custom_defconfig
```

To re-configure the kernel config files:

```
make menuconfig
make savedefconfig
```

To ensure the configure files will be used by my scripts:

```
cp .config ../$(basename $(pwd)).config
cp defconfig ../$(basename $(pwd)).defconfig
```

To make the kernel and their associated kernel modules manually:

```
make -j8 zImage
make -j8 modules
[ -d arch/arm/boot/dts ] && make -j8 dtbs
[ ! -d modules ] && mkdir -p modules
make INSTALL_MOD_PATH=./modules modules_install
```

If the kernel supports dtb (i.e. embedded device kernel) and has
CONFIG_ARM_APPENDED_DTB=y, then you will need to concatenate the
kernel image and the dtb together.

```
cat arch/arm/boot/zImage arch/arm/boot/dts/rk3188-radxarock.dtb > arch/arm/boot/zImage-dtb
```

Once the kernel and modules are built, you can create the flash
partition images for the Radxa Rock partition by using the
rkflashtool:

```
../rkflashtool/rkcrc -k arch/arm/boot/dts/rk3188-radxarock.dtb ../build/kernel-$(basename $(pwd)).img
```

## Flashing the images

```
./flasher.sh linux-stable
```

Device Drivers
  * MMC/SD/SDIO card support
    * Synopsys Designware Memory Card Interface
      * Rockchip specific extensions for Synopsys DW Memory Card Interface 
        * enabling it results in kernel panic
        * seems to be a bug in dwmmc_rockchip module in 3.18y kernel series
