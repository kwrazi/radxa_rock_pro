#!/bin/bash
#
# Kiet To
# 13th May 2015
#

. scripts/build.sh

TOOLDIR="rkflashtool"
FLASHER="${TOOLDIR}/rkflashtool"
UPTOOL="Linux_Upgrade_Tool_v1.21/upgrade_tool"

function die {
    echo "$*"
    exit 1
}

function flasher {
    [ ! -z "$1" ] && OFFSET="$1" || die "flasher: no offset parameter"
    [ ! -z "$2" ] && SIZE="$2" || die "flasher: no size parameter"
    [ ! -z "$3" ] && IMAGE="$3" || die "flasher: no image parameter"
    TOOLDIR="rkflashtool"
    FLASHER="${TOOLDIR}/rkflashtool"
    BACKUP="${IMAGE}.bak"
    echo "backing up ofs:${OFFSET} sz:${SIZE} to ${BACKUP}"
    ${FLASHER} r ${OFFSET} ${SIZE} > ${BACKUP}
    if [ -f ${IMAGE} ]; then
	${FLASHER} w ${OFFSET} ${SIZE} < ${IMAGE}
    else
	echo "${IMAGE} does not exists"
    fi
}

function flash_images {
    # assuming parameter_dual_sd is used
    #   Part       Offset       Size
    #   ------     --------     ------
    #   misc       0x2000       0x2000
    #   kernel     0x4000       0x8000
    #   boot       0x8000       0x8000
    #   recovery   0x10000      0x10000
    #   cache      0x40000      0x40000
    #   userdata   0x80000      0x200000
    #   kpanic     0x280000     0x2000
    #   system     0x282000     0x100000
    #   linuxroot  0x382000     0x300000

    # using rkflashtool
    # if [ -d "${BUILD}" ]; then
    # 	flasher 0x0 0x2 ${BUILD}/param.img
    # 	flasher 0x4000 0x8000 ${BUILD}/kernel.img
    # 	flasher 0x8000 0x8000 ${BUILD}/boot.img
    # 	flasher 0x10000 0x10000 ${BUILD}/recovery.img
    # fi
    if [ -d "${BUILD}" ]; then
	sudo upgrade_tool di boot ${BUILD}/boot.img
	sudo upgrade_tool di kernel ${BUILD}/kernel.img
	sudo upgrade_tool di parameter ${BUILD}/parameter
    fi
}

function in_recovery_mode {
    sudo ${FLASHER} v 2> /dev/null
    return $?
}

function extract_parameters {
    PARAM="${BUILD}/current_parameters.txt"
    if in_recovery_mode; then
	echo "Writing Rockchip parameters into ${PARAM}..."
	sudo ${FLASHER} p 2> /dev/null > ${PARAM}
    else
	echo "Rockchip is not in recovery mode. "
	echo "Put it in recovery mode and then try again."
	exit 1
    fi
}

function flash_firmware {
    [ -n "$1" ] && IMAGE="$1"
    if [ -f ${IMAGE} ]; then
	sudo ${UPTOOL} uf ${IMAGE}
    fi
}

function flash_parameter {
    [ -n "$1" ] && PARAM="$1"
    if [ -f ${PARAM} ]; then
	echo "----> flashing ${PARAM}"
	sudo ${UPTOOL} di -p ${PARAM}
    fi
}

function flash_kernel {
    [ -n "$1" ] && KERNEL="$1"
    if [ -f ${KERNEL} ]; then
	echo "----> flashing ${KERNEL}"
	sudo ${UPTOOL} di -k ${KERNEL}
    fi
}

function flash_boot {
    [ -n "$1" ] && BOOT="$1"
    if [ -f ${BOOT} ]; then
	echo "----> flashing ${BOOT}"
	sudo ${UPTOOL} di -b ${BOOT}
    fi
}

function copy_modules {
    [ -n "$1" ] && MODULEDIR="$1"
    DEST=/media/linuxroot/lib/modules
    if [ -d ${MODULEDIR} ]; then
	echo "sudo rsync -av ${MODULEDIR} ${DEST}/"
	echo "sudo chown -R root:root ${DEST}/*"
    fi
}

function help {
    echo "Syntax: $0 <kernelpath>"
    exit 0
}

[ -n "$1" ] && LINUXDIR="$1" || help
if sudo ${UPTOOL} td | grep "Test Device OK."; then
    #ANDROID=radxa_rock_pro_android_kitkat_hdmi_20150403_nand
    #ANDROID_IMG=${ANDROID}/radxa_rock_pro_android_kitkat_hdmi_20150403_nand.img
    #flash_firmware ${ANDROID_IMG}
    #flash_parameter parameter/parameter_dual_sd
    #flash_parameter parameter/parameter_dual_sd_debug
    flash_kernel ${BUILD}/kernel-${LINUXDIR}.img
    flash_boot ${BUILD}/boot-${LINUXDIR}.img
    #copy_modules ${LINUXDIR}/modules/lib/modules/4.1.0-rc3+
fi
