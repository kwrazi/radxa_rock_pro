#!/bin/bash
#
# Kiet To
# 13th May 2015
#

KERNEL=$(basename $0 .sh)

. scripts/toolchain.sh
. scripts/downloads.sh
. scripts/build.sh
[ ! -f scripts/kernel_$(basename $0) ] && exit $?
. scripts/kernel_$(basename $0) 

# Clean build
rm -v ${BUILD}/param-${KERNEL}.img
rm -v ${BUILD}/kernel-${KERNEL}.img
rm -v ${BUILD}/boot-${KERNEL}.img

# For linux kernel build
ubuntu_install_packages git build-essential lzop libncurses5-dev libssl-dev bc
# For rkflashtool build
ubuntu_install_packages libusb-1.0-0 libusb-1.0-0-dev
# grab the linux source tree from git repos
get_source
# build linux kernel
build_kernel "${KERNEL}"
# build mkbootimg tool
build_mkbootimg
# build rkflashtool tool
build_rkflashtool
# build images
build_rockchip_images "${KERNEL}"
build_boot_image "${KERNEL}"

ls -la ${BUILD}/*-${KERNEL}.img
