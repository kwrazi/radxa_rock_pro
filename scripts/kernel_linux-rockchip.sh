#!/bin/bash
#
# Kiet To
# 15th May 2015
#

if [ -z "${DOWNLOADS}" ]; then
    echo "source scripts/downloads.sh before using functions in this script"
    exit 1
fi
[ ! -d "${DOWNLOADS}" ] && mkdir -pv "${DOWNLOADS}"

function get_source {
    URL="https://github.com/radxa/linux-rockchip.git"
    DIR=$(basename "${URL}" .git)
    BRANCHPATH="origin/radxa-rbox"
    BRANCH=$(basename ${BRANCHPATH})

    echo "Cloning ${DIR} source..."
    clone "${URL3}" "${BRANCHPATH}"
    echo "---- Done ----"
}

function make_defconfig {
    FILE="arch/arm/configs/radxa_rock_pro_lite_hdmi_defconfig"
    DEFCONFIG=$(basename "${FILE}")

    echo "  Making config=${DEFCONFIG} for radxa board..."
    if [ -f "${FILE}" ]; then
	make ${DEFCONFIG}
    fi
}
