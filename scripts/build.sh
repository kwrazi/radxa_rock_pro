#!/bin/bash
#
# Kiet To
# 14th May 2015
#

BUILD="build"

function ubuntu_install_packages {
    echo "Installing packages to Ubuntu..."
    sudo apt-get install $*
} 

function make_defconfig {
    echo "  Grabbing and making defconfig for rockchip board..."
    FILE="arch/arm/configs/rockchip_defconfig"
    if [ ! -f "${FILE}" ]; then
     	wget http://rockchip.fr/radxa/linux/rockchip_defconfig -O "${FILE}"
    fi
    if [ -f "${FILE}" ]; then
	make $(basename "${FILE}")
    fi
}

function get_dts {
    URL="http://rockchip.fr/radxa/linux/rk3188-radxarock.dts"
    FILE="arch/arm/boot/dts/rk3188-radxarock.dts"
    DIR=$(dirname "${FILE}")

    echo "  Grabbing device tree source (dts).."
    if [ -d "${DIR}" ]; then
	if [ ! -f "${FILE}" ]; then
	    wget "${URL}" -O "${FILE}" || exit $?
	fi
    else
	echo "  WARNING: kernel does not support dts files"
    fi
}

function make_dtb {
    FILE="arch/arm/boot/dts/rk3188-radxarock.dts"
    DIR=$(dirname "${FILE}")
    
    echo "  Making dtbs..."
    if [ -d "${DIR}" ]; then
	make -j8 dtbs
	
	ZIMAGE="arch/arm/boot/zImage"
	DTB="arch/arm/boot/dts/rk3188-radxarock.dtb"
	DTBIMAGE="arch/arm/boot/zImage-dtb"
	if [ -f "${ZIMAGE}" ] && [ -f "${DTB}" ]; then 
	    echo "  Creating ${DTBIMAGE}..."
	    cat "${ZIMAGE}" "${DTB}" > "${DTBIMAGE}"
	else
	    echo "  DTB or zImage file missing. Skipping zImage-dtb creation."
	fi
    else
	echo "  WARNING: kernel does not support dts files"
    fi
}

function build_kernel {
    [ ! -z "$1" ] && KERNEL="$1" || return 1
    pushd ${KERNEL} > /dev/null

    make_defconfig
    get_dts

    # make menuconfig 

    # Enable:
    #   <M> Device Drivers -> USB support -> USB Serial Converter Support 
    #          -> USB FTDI Single Port Serial Driver
    #   <M> File systems -> Reiserfs support
    #   <M> File systems -> JFS filesystem support
    #   <M> File systems -> XFS filesystem support
    #   <M> File systems -> Btrfs filesystem support
    #   <M> File systems -> Network File Systems -> CIFS suport

    make -j8 zImage
    make_dtb

    [ ! -d modules ] && mkdir -pv modules
    make -j8 modules
    make INSTALL_MOD_PATH=./modules modules_install
    popd > /dev/null
}

function generic_build {
    [ ! -z "$1" ] && DIR="$1" || return 1
    [ ! -z "$2" ] && INSTALLKIT="$2" || return 1
    echo "Building ${DIR}..."
    if [ ! -d "${DIR}" ]; then
	${INSTALLKIT}
    fi
    if [ -f "${DIR}/Makefile" ]; then
	pushd "${DIR}" > /dev/null
	make -j8 || exit $?
	popd > /dev/null
    else
	echo "  Nothing to build for ${DIR}..."
    fi
}

function build_rkflashtool {
    generic_build rkflashtool get_rockchip_tools
}

function build_mkbootimg {
    generic_build rockchip-mkbootimg get_rockchip_tools
}

function build_rockchip_images {
    [ ! -z "$1" ] && KERNEL="$1" || return 1
    DIR=rkflashtool
    [ ! -d "parameter" ] && get_rockchip_parameters
    echo "Building Radxa Rockchip images for flashing..."
    pushd "${DIR}" > /dev/null
    SRC="../parameter/parameter_dual_sd"
    PARAMIMG=../"${BUILD}/param-${KERNEL}.img"
    if [ ! -f "${PARAMIMG}" ]; then
	./rkcrc -p "${SRC}" "${PARAMIMG}"
    fi
    [ -f ../${KERNEL}/arch/arm/boot/zImage-dtb ] && SRC=../${KERNEL}/arch/arm/boot/zImage-dtb || SRC=../${KERNEL}/arch/arm/boot/zImage
    KRNLIMG=../"${BUILD}/kernel-${KERNEL}.img"
    if [ ! -f "${KRNLIMG}" ]; then
	./rkcrc -k "${SRC}" "${KRNLIMG}"
    fi
    popd > /dev/null
}

function build_initrd {
    echo "Building initrd recovery images for flashing..."
    generic_build initrd get_rockchip_tools
    FILE="initrd.img"
    [ -f "${FILE}" ] && mv -v "${FILE}" "${BUILD}"
}

function build_boot_image {
    [ ! -z "$1" ] && KERNEL="$1" || return 1
    echo "Building boot images for flashing..."
    ZIMAGE="${KERNEL}/arch/arm/boot/zImage"
    DTBIMAGE="${KERNEL}/arch/arm/boot/zImage-dtb"
    if [ -f "${DTBIMAGE}" ]; then
	ZIMAGE=${DTBIMAGE}
    fi
    RAMDISK=/dev/null
    BOOTIMG="${BUILD}/boot-${KERNEL}.img"
    
    rockchip-mkbootimg/mkbootimg --kernel "${ZIMAGE}" --ramdisk ${RAMDISK} \
	-o "${BOOTIMG}"
}

PKGNAME="scripts/build.sh"
if [ $(basename "$0") = "${PKGNAME}" ]; then
    echo "Don't run this script directly. Source it."
else
    [ ! -d "${BUILD}" ] && mkdir -pv "${BUILD}";
    echo "Loading build functions from ${PKGNAME}..."
fi
