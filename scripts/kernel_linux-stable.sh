#!/bin/bash
#
# Kiet To
# 15th May 2015
#

if [ -z "${DOWNLOADS}" ]; then
    echo "source scripts/downloads.sh before using functions in this script"
    exit 1
fi
[ ! -d "${DOWNLOADS}" ] && mkdir -pv "${DOWNLOADS}"

function get_source {
    URL="git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git"
    DIR=$(basename "${URL}" .git)
    BRANCHPATH="origin/linux-3.18.y"
    BRANCH=$(basename ${BRANCHPATH})

    echo "Cloning ${DIR} source..."
    clone "${URL3}" "${BRANCHPATH}"
    echo "---- Done ----"
}

function make_defconfig {
    URL="http://rockchip.fr/radxa/linux/rockchip_defconfig"
    DEFCONFIG=$(basename "${URL}")
    FILE="arch/arm/configs/${DEFCONFIG}"
    
    LOCAL_CONFIG=../$(basename $(pwd)).config
    LOCAL_DEFCONFIG=../$(basename $(pwd)).defconfig
    if [ -f "${LOCAL_DEFCONFIG}" ]; then
	cp -v ${LOCAL_DEFCONFIG} arch/arm/configs
	make ${LOCAL_DEFCONFIG}
    else
	echo "  Grabbing and making config=${DEFCONFIG} for rockchip board..."
	if [ ! -f "${FILE}" ]; then
     	    wget "${URL}" -O "${FILE}" || exit $?
	fi
	if [ -f "${FILE}" ]; then
	    make ${DEFCONFIG}
	else 
	    exit 1
	fi
    fi
    make menuconfig
    make savedefconfig
    cp -v .config "${LOCAL_CONFIG}"
    cp -v defconfig "${LOCAL_DEFCONFIG}"
}
