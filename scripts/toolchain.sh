#!/bin/bash
#
# source this file to import environment variables
#
export ARCH=arm
export CROSS_COMPILE=/home/kto/src/radxa_rock_pro/x86_64_arm-eabi-4.6/arm-eabi-4.6/bin/arm-eabi-

PKGNAME="toolchain.sh"
if [ $(basename "$0") = "${PKGNAME}" ]; then
    echo "Don't run this script directly. Source it."
else
    echo "Setting up ARM cross compile toolchain..."
fi
