#!/bin/bash
#
# Kiet To
# 15th May 2015
#

if [ -z "${DOWNLOADS}" ]; then
    echo "source scripts/downloads.sh before using functions in this script"
    exit 1
fi
[ ! -d "${DOWNLOADS}" ] && mkdir -pv "${DOWNLOADS}"

function get_source {
    URL="git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git"
    DIR=$(basename "${URL}" .git)
    BRANCHPATH="origin/master"
    BRANCH=$(basename ${BRANCHPATH})

    echo "Cloning ${DIR} source..."
    clone "${URL3}" "${BRANCHPATH}"
    echo "---- Done ----"
}

function make_defconfig {
    URL="http://rockchip.fr/radxa/linux/rockchip_defconfig"
    URL="https://raw.githubusercontent.com/radxa/linux-rockchip/radxa-rbox/arch/arm/configs/radxa_rock_pro_lite_hdmi_defconfig"
    DEFCONFIG=$(basename "${URL}")
    FILE="arch/arm/configs/${DEFCONFIG}"

    CONFIG=$(basename $(pwd)).config
    if [ -f ../"${CONFIG}" ]; then
	echo "Using custom config ${CONFIG} settings..."
	cp -v ../"${CONFIG}" .config
    else
	echo "  Grabbing and making config=${DEFCONFIG} for rockchip board..."
    	wget "${URL}" -O "${FILE}" || exit $?
	if [ -f "${FILE}" ]; then
	    make ${DEFCONFIG}
	fi
    fi
}
