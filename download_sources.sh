#!/bin/bash
#
# Kiet To
# 13th May 2015
#

DOWNLOADS="downloads"

function download {
    URL="$1"
    [ -z "${URL}" ] && return 1
    FILE="${DOWNLOADS}"/$(basename "$URL")
    if [ ! -z "$2" ]; then
	DIR="$2"
    else
	DIR=$(basename "${FILE}" | sed -e 's/\.[^\.]*$//')
    fi
    if [ ! -f "${FILE}" ]; then
	echo "  Downloading ${FILE}..."
	wget "${URL}" -P "${DOWNLOADS}" || exit $?
    fi
    if [ ! -d "${DIR}" ]; then
	mkdir -pv "${DIR}"
    fi
    case ${FILE##*.} in
	"zip" )
	    echo "  Decompressing ${FILE} to '${DIR}/'..."
	    unzip -n "${FILE}" -d "${DIR}" || exit $?
	    ;;
	".gz" | ".tgz" )
	    echo "  Decompressing ${FILE} to '${DIR}/'..."
	    tar zxvf "${FILE}" -C "${DIR}" || exit $?
	    ;;
    esac
}

function download_directory {
    URL="$1"
    [ -z "${URL}" ] && return 1
    [ ! -z "$2" ] && DIR="$2" || DIR=$(basename "$URL")
    if [ ! -d "${DIR}" ]; then
	echo "  Downloading ${FILE}..."
	wget -mirror -nH --no-parent --reject "index.htm*" "${URL}" -P "${DIR}" || exit $?
    fi
}

function clone {
    URL="$1"
    [ -z "${URL}" ] && return 1
    DIR=$(basename "$URL" .git)
    [ ! -z "$2" ] && BRANCH="$2" || BRANCH="origin/master"
    if [ ! -d "${DIR}" ]; then
	echo "  Cloning ${DIR}"
	git clone --depth 1 "${URL}" || exit $?
	pushd "${DIR}" > /dev/null
	[ ! -z "$BRANCH" ] && git checkout $(basename "$BRANCH")
	popd > /dev/null
    else
	pushd "${DIR}" > /dev/null
	echo "  Clone ${DIR} exists. Updating git clone instead."
	if [ ! -z "$BRANCH" ]; then
	    git pull $(dirname "$BRANCH") $(basename "$BRANCH") || exit $?
	else
	    git pull || exit $?
	fi
	popd > /dev/null
    fi
}

function get_toolchain {
    echo "Grabbing pre-build gcc cross-compile toolchain..."
    URL="http://dl.radxa.com/rock/source/x86_64_arm-eabi-4.6.zip"
    DIR=$(basename "${URL}" .zip)
    download "${URL}"
    echo "---- Done ----"
}

function get_prebuilt_images {
    echo "Grabbing pre-built images for Radxa Rock Pro..."
    URLs=( \
	"http://dl.radxa.com/rock_pro/images/ubuntu/sd/radxa_rock_pro_ubuntu_14.04_server_k318rc5_141129_sdcard.zip" \
	"http://dl.radxa.com/rock_pro/images/ubuntu/sd/radxa_rock_pro_lite_ubuntu_14.04_server_141030_sdcard.zip" \
	"http://dl.radxa.com/rock_pro/images/android/radxa_rock_pro_android_kitkat_hdmi_20150403_nand.zip" \
	)
    for ((i = 0; i < ${#URLs[@]}; i++)); do
	download "${URLs[$i]}"
    done
}

function get_ubuntu_build {
    echo "Grabbing Ubuntu 14.10 Linaro build sources..."
    URL="https://releases.linaro.org/latest/ubuntu/utopic-images/alip"
    DIR="ubuntu_build"
    download_directory "${URL}" "${DIR}"
    echo "---- Done ----"
}

function get_linux_sources {
    echo "Grabbing shallow clone of linux sources..."
    URL1="git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git"
    clone "${URL1}" "origin/linux-3.18.y"
    URL2="git://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git"
    clone "${URL2}" "origin/stable"
    URL3="https://github.com/radxa/linux-rockchip.git"
    clone "${URL3}" "origin/radxa-stable-3.0"
    echo "---- Done ----"
}

function get_bootloaders {
    echo "Grabbing Radxa Rockchip bootloader images..."
    # latest from http://dl.radxa.com/rock/images/loader/
    URLs=( \
	"http://dl.radxa.com/rock/images/loader/RK3188Loader(L)_V2.19.bin" \
	"http://dl.radxa.com/rock/images/loader/RK3188Loader(L)_V2.19.bin.md5" \
	"http://dl.radxa.com/rock/images/loader/RK3188Loader(L)_V2.10.bin" \
	"http://dl.radxa.com/rock/images/loader/RK3188Loader(L)_V2.10.bin.md5" \
	)
    DIR="loaders"
    for ((i = 0; i < ${#URLs[@]}; i++)); do
	download "${URLs[$i]}" "${DIR}"
    done
    echo "---- Done ----"
}

function get_rockchip_parameters {
    echo "Grabbing Radxa Rockchip boot loader parameter files..."
    URL=http://dl.radxa.com/rock/images/parameter/parameters_all.zip
    DIR="parameter"
    download "${URL}" "${DIR}"
    echo "---- Done ----"
}

function get_rockchip_tools {
    echo "Grabbing Radxa/Rockchip tools..."
    URLs=( \
	"http://dl.radxa.com/rock_pro/tools/linux/Linux_Upgrade_Tool_v1.21.zip" \
	)
    DSTs=( "." )
    for ((i = 0; i < ${#URLs[@]}; i++)); do
	if [ ! -z "${DSTs[$i]}" ]; then
	    if [ -d "${DSTs[$i]}" ]; then
		download "${URLs[$i]}" "${DSTs[$i]}"
	    else
		echo "  Directory "${DSTs[$i]}" not found"
	    fi
	else
	    download "${URLs[$i]}"
	fi
    done
    GITs=( \
	"https://github.com/neo-technologies/rockchip-mkbootimg.git" \
	"https://github.com/linux-rockchip/rkflashtool" \
	"https://github.com/radxa/initrd" \
	"https://github.com/radxa/rockchip-pack-tools.git" \
	"https://github.com/radxa/pyRock.git" \
	)
    for ((i = 0; i < ${#GITs[@]}; i++)); do
	clone "${GITs[$i]}"
    done
    echo "---- Done ----"
}

if [ $(basename $0) = "download_sources.sh" ]; then
    [ ! -d "${DOWNLOADS}" ] && mkdir -pv "${DOWNLOADS}";

    get_toolchain
    get_prebuilt_images
    get_linux_sources
    get_bootloaders
    get_ubuntu_build
    get_rockchip_parameters
    get_rockchip_tools
else
    echo "download_sources functions loaded by $0"
fi
